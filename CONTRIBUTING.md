** parameters

>-f  [ Input file - Required ] 
>--sr [ Output path - Required ] 
>--tp  [ Directory of temporary file - Required ] 
>--uid  [ MGB user Id - Required ] 
>--rn  [ Report Number - Required ] 


usage : python ____.py -f [Genotype or vcf file] --sr [path] --tp [path] --uid [int] --rn [int]
