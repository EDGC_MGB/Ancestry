import os
import sys
import getopt
import re
#import traceback


APP_NAME='Ancestry-3.4.1'

def printParams():
	for parameter_val in Messege_print:
		print parameter_val

def printHelp():
	print '\n** parameters of '+APP_NAME+' are : '
	print '-f <Input file (Genotype file or VCF file) - Required >'
	print '--sr <Output path - Required>'
	print '--tp <Directory of temporary file - Required>'
	print '--uid <MGB user Id - Required>'
	print '--rn <Report Number - Required>' 
	print '--rl <Report language - Optional>\n'

def checkReportNum():
	if Report_num.isspace() or len(Report_num) == 0 or Report_num.startswith('--'):
		print '\nReport_num is empty! so cannot start '+APP_NAME
		printHelp()
		sys.exit()

def exit(msg):
	print 'aler^' + Report_num + '^' + Err_m[1] + ' ' + msg+'^alend;'
	sys.exit()

def checkParams():
	errs=[]

	def plus(m):
		errs.append(Report_num + '!' +m)

	if not os.path.isfile(Input_f):
		plus('invalid input file (-f : '+Input_f+')')

	if not os.path.isdir(Temp_dir):
		plus('invalid or inaccessible path (--tp : '+Temp_dir+')')

	if not os.path.isdir(Output_dir):
		plus('invalid or inaccessible path (--sr : '+Output_dir+')')

	if not User_id or not User_id.strip():
		plus('User_id is empty!')

	if len(errs) > 0:
		for err in errs:
			print err
		exit(str(len(errs)) + ' parameter error(s).')

def exitNotEnoughParam():
	exit('Not enough Parameter(s)')

def natural_sort(l):
	convert = lambda text: int(text) if text.isdigit() else text.lower()
	alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key)]
	return sorted(l, key = alphanum_key)

RUN_HOME=sys.path[0]

Err_m=[]
Error_messege_file=open(RUN_HOME+"/Error_messege_MGBApp.txt","r")
for line in Error_messege_file:
	Err_m.append(line.strip())

Report_num=''

##================================================================================================##
## Parameter
##================================================================================================##

try:
	if len(sys.argv[1:]) < 1:
		printHelp()
		sys.exit()

	paramList=sys.argv[1:]
	rn_idx=paramList.index('--rn')

	if rn_idx+1 > -1 and rn_idx <= len(paramList)-1:
		Report_num=paramList[rn_idx+1]
		checkReportNum()

	input_f=[]
	user_id=[]
	report_num=[]
	temp_dir=[]
	output_dir=[]

	Messege_print=[]

	def main(argv):
		try:
			opts, args = getopt.getopt(sys.argv[1:], "f:", ["uid=","rn=","rl=","sr=","tp=","rl="])

		except getopt.GetoptError:
			printHelp()
			exitNotEnoughParam()

		for opt, arg in opts:
			if opt in '-f':
				input_f.append(arg)
				Messege_print.append('f^'+arg)

			elif opt in '--uid':
				user_id.append(arg)
				Messege_print.append('uid^'+arg)

			elif opt in '--tp':
				arg=arg.rstrip('/')
				Messege_print.append('tp^'+arg)
				temp_dir.append(arg)

			elif opt in '--sr':
				arg=arg.rstrip('/')
				Messege_print.append('sr^'+arg)
				output_dir.append(arg)

			elif opt in '--rl':
				Messege_print.append('rl^'+arg)

	if __name__ == "__main__":
	   main(sys.argv[1:])

	Input_f=str("".join(input_f).strip())
	User_id=str("".join(user_id).strip())
	Temp_dir=str("".join(temp_dir).strip())
	Output_dir=str("".join(output_dir).strip())

	#CHECK Report_num first!
	checkReportNum()
	print 'alst^'+ Report_num + '^start^alend;'
	printParams()

	checkParams()

	##==============================================================================================================================================##
	##==============================================================================================================================================##
	## Step0. Converting Genotype to VCF format
	##==============================================================================================================================================##
	##==============================================================================================================================================##

	#1000genome dat
	dat_1000g=open(RUN_HOME+"/af_GSA_std_cus.txt","r")
	g1000_dic={}
	g1000_rsid={}
	for line in dat_1000g:
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			Chr, Loci, ref_rsid, Ref, Alt = ls[0].split("chr")[1], ls[1], ls[4], ls[2], ls[3]
			db_key = Chr +"/"+ Loci +"/"+ ref_rsid +"/"+ Alt
			g1000_dic[db_key] = Chr+"\t"+Loci+"\t"+ref_rsid+"\t"+Ref+"\t"+Alt
			g1000_rsid[ref_rsid]=0

	dat_1000g.close()

	## Alfred database 
	db_rsid={}
	for line in open(RUN_HOME+"/GSA_POP24_alfred_parsing_ref.txt", "r"):
		if not line.startswith("population"):
	 		ls=line.strip().split("\t")
			RSID = ls[2]
			db_rsid[RSID]=0

	for line2 in open(RUN_HOME+"/GSA_POP24_alfred_parsing_alt.txt", "r"):
		if not line2.startswith("population"):
			ls=line2.strip().split("\t")
			RSID = ls[2]
			db_rsid[RSID]=0

	#===============================================================================================================================
	# if GT file
	#===============================================================================================================================
	if not ".vcf" in Input_f: #GT file
		#GT_dat=open(Input_f,"r")
		GT_rsid={}
		for line in open(Input_f,"r"):
			if not line.startswith("#"):
				ls=line.strip().split("\t")
				if len(ls[3].strip()) == 2 and ls[0].startswith("rs"):
					RSID = ls[0]
					GT_rsid[RSID]=0

		GT_rs_mat1=0
		GT_rs_mat2=0
		for gt_rsid in GT_rsid:
			if gt_rsid in g1000_rsid:
				GT_rs_mat1+=1
			if gt_rsid in db_rsid:
				GT_rs_mat2+=1

		if GT_rs_mat1 < 80000:
			#print "Common marker : ", GT_rs_mat1
			#print "QC Fail / it is less marker than 1000Genome ... ( x < 90000)"
			print 'alca^' + Report_num + '^' +Err_m[0]+' QC_Fail' + '^alend;'
			sys.exit()
		if GT_rs_mat2 < 50000:
			#print "Common marker : ", GT_rs_mat2
			#print "QC Fail / it is less marker than Alfred ... ( x < 70000)"
			print 'alca^' + Report_num + '^' +Err_m[0]+' QC_Fail' + '^alend;'
			sys.exit()


		Sample_id = Input_f.split("/")[-1].split(".txt")[0]
		OUT_vcf=open(Temp_dir+"/"+Sample_id+".procesed.vcf","w")
		Alt_Homo={}
		for line in open(Input_f,"r"):
			if not line.startswith("#"):
				ls=line.strip().split("\t")
				if len(ls[3].strip()) == 2:
					RSID, CHR, POS, GT_allele1, GT_allele2 = ls[0], ls[1], ls[2], ls[3].strip()[0], ls[3].strip()[1]
					if CHR.startswith("chr"):
						CHR=CHR.split("chr")[1]
					if GT_allele1 != GT_allele2:
						Key1 = CHR +"/"+ POS +"/"+RSID+"/"+ GT_allele1
						Key2 = CHR +"/"+ POS +"/"+RSID+"/"+ GT_allele2
						if Key1 in g1000_dic : #0/1
							vcf_format="\t".join(g1000_dic[Key1].split("\t"))+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+"0/1:"+"\n"
							OUT_vcf.write(vcf_format)

						elif Key2 in g1000_dic :
							vcf_format="\t".join(g1000_dic[Key2].split("\t"))+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+"0/1:"+"\n"
							OUT_vcf.write(vcf_format)

						else:
							pass
					else:
						Key = CHR +"/"+ POS +"/"+RSID+"/"+ GT_allele1
						if Key in g1000_dic: #1/1
							vcf_format="\t".join(g1000_dic[Key].split("\t"))+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+"1/1:"+"\n"
							OUT_vcf.write(vcf_format)
		OUT_vcf.close()

		temp_Vcf=str(Temp_dir+"/"+Sample_id+".procesed.vcf")

		os.system("sort -k1,1V -k2,2n "+ Temp_dir+"/"+Sample_id+".procesed.vcf" + " > "+ Temp_dir+"/"+Sample_id+".ordered.procesed.vcf")
		os.system("cat "+RUN_HOME+"/vcf_header.txt "+ Temp_dir+"/"+Sample_id+".ordered.procesed.vcf" +" > "+Temp_dir+"/"+Sample_id+".final.vcf")

		os.system("rm "+ Temp_dir+"/"+Sample_id+".procesed.vcf")
		os.system("rm "+ Temp_dir+"/"+Sample_id+".ordered.procesed.vcf")

	#===============================================================================================================================
	# if VCF file
	#===============================================================================================================================

	if ".vcf" in Input_f:
		#VCF_dat=open(Input_f,"r")
		VCF_RSID={}
		total_vcfLine=0
		with open(Input_f,"r") as f:
			for line in f:
				if not line.startswith("#"):
					total_vcfLine+=1
					ls=line.strip().split("\t")
					if len(ls) == 10 and ls[2].startswith("rs"):
						RSID = ls[2]
						VCF_RSID[RSID]=0
		
		if float(len(VCF_RSID))/float(total_vcfLine) < 0.5:
			vcf_dic={}
			check_RefVersion=[]
			with open(Input_f,"r") as f:
				for line in f:
					if line.startswith("#"):
						check_RefVersion.append(line.strip())
					else:
						ls=line.strip().split("\t")
						Chr, Pos, Ref, Alt = ls[0], ls[1], ls[3], ls[4]
						if Chr.startswith("chr"):
							Chr=ls[0].split("chr")[1]
						for alt in Alt.split(","):
							vcf_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt]="\t".join(ls[5:])
			

			VCF_version=""
			for val_line in check_RefVersion:
				if "hg38" in val_line.lower() or "grch38" in val_line.lower():
					VCF_version="hg38"
					break
				elif "hg19" in val_line.lower() or "grch37" in val_line.lower():
					VCF_version="hg19"
					break

			#print VCF_version

			if VCF_version=="hg19":
				dbmatching_dic={}
				with open(RUN_HOME+"/RAW_661638_dbsnp_147.hg19.vcf", "r") as dbsnp_data:
					for line in dbsnp_data:
						if not line.startswith("#"):
							ls=line.strip().split("\t")
							Chr, Pos, RSID, Ref, Alt = ls[0], ls[1], ls[2], ls[3], ls[4]
							for alt in Alt.split(","):
								if Chr+"\t"+Pos+"\t"+Ref+"\t"+alt in vcf_dic:
									VCF_RSID[RSID]=0
									if not Chr+"\t"+Pos+"\t"+Ref+"\t"+alt in dbmatching_dic:
										dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt]=[]
									dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt].append(RSID)

				out_mat_vcf=open(Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf", "w")
				
				with open(Input_f,"r") as f:
					for line in f:
						if line.startswith("#"):
							out_mat_vcf.write(line)
						else:
							ls=line.strip().split("\t")
							Chr, Pos, Ref, Alt = ls[0], ls[1], ls[3], ls[4]
							if Chr.startswith("chr"):
								Chr=ls[0].split("chr")[1]
							if Chr+"\t"+Pos+"\t"+Ref+"\t"+Alt in dbmatching_dic:
								for val in dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+Alt]:
									out_mat_vcf.write(Chr+"\t"+Pos+"\t"+val+"\t"+"\t".join(ls[3:])+"\n")
				out_mat_vcf.close()

				Input_f=Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf"

			else: #version is hg38!
				dbmatching_dic={}
				with open(RUN_HOME+"/Ancestry740k_dbsnp_hg38.vcf", "r") as dbsnp_data:
					for line in dbsnp_data:
						if not line.startswith("#"):
							ls=line.strip().split("\t")
							Chr, Pos, RSID, Ref, Alt = ls[0], ls[1], ls[2], ls[3], ls[4]
							for alt in Alt.split(","):
								if Chr+"\t"+Pos+"\t"+Ref+"\t"+alt in vcf_dic:
									VCF_RSID[RSID]=0
									if not Chr+"\t"+Pos+"\t"+Ref+"\t"+alt in dbmatching_dic:
										dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt]=[]
									dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt].append(RSID)

				out_mat_vcf=open(Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf", "w")
				
				with open(Input_f,"r") as f:
					for line in f:
						if line.startswith("#"):
							out_mat_vcf.write(line)
						else:
							ls=line.strip().split("\t")
							Chr, Pos, Ref, Alt = ls[0], ls[1], ls[3], ls[4]
							if Chr.startswith("chr"):
								Chr=ls[0].split("chr")[1]
							if Chr+"\t"+Pos+"\t"+Ref+"\t"+Alt in dbmatching_dic:
								for val in dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+Alt]:
									out_mat_vcf.write(Chr+"\t"+Pos+"\t"+val+"\t"+"\t".join(ls[3:])+"\n")
				out_mat_vcf.close()

				Input_f=Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf"

		vcf_rs_mat1=0
		vcf_rs_mat2=0
		for vcf_rsid in VCF_RSID:
			if vcf_rsid in g1000_rsid:
				vcf_rs_mat1+=1
			if vcf_rsid in db_rsid:
				vcf_rs_mat2+=1

		if vcf_rs_mat1 < 80000:
			#print "Common marker : ", vcf_rs_mat1
			#print "QC Fail / it is less marker than 1000Genome ... ( x < 90000)"
			os.system("rm -f "+Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0].split("_mat")[0]+"_mat.vcf")
			print 'alca^' + Report_num + '^' + Err_m[0]+' QC_Fail' + '^alend;'
			sys.exit()

		if vcf_rs_mat2 < 50000:
			#print "Common marker : ", vcf_rs_mat2
			#print "QC Fail / it is less marker than Alfred ... ( x < 70000)"
			os.system("rm -f "+Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0].split("_mat")[0]+"_mat.vcf")
			print 'alca^' + Report_num + '^' + Err_m[0]+' QC_Fail' + '^alend;'
			sys.exit()

		Sample_id = Input_f.split("/")[-1].split(".vcf")[0]
		OUT_vcf=open(Temp_dir+"/"+Sample_id+".final.vcf","w")
		for line in open(Input_f,"r"):
			if line.startswith("#"):
				OUT_vcf.write(line)
			else:
				ls=line.strip().split("\t")
				if len(ls) == 10:
					if ls[9].strip().split(":")[0] in ["1/1","0/1"]:
						if ls[6]=="." and ls[7]==".":
							if ls[0].startswith("chr"): ## Convert character "chr1" to "1"
								OUT_vcf.write(ls[0].split("chr")[1]+"\t"+"\t".join(ls[1:5])+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+ls[9].strip().split(":")[0]+":"+"\n")
							else:
								OUT_vcf.write("\t".join(ls[:5])+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+ls[9].strip().split(":")[0]+":"+"\n")
						else:
							if ls[0].startswith("chr"):
								OUT_vcf.write(ls[0].split("chr")[1]+"\t"+"\t".join(ls[1:])+"\n") ## Convert character "chr1" to "1"
							else:
								OUT_vcf.write("\t".join(ls[0:])+"\n") 
		OUT_vcf.close()

	## setting Eagles
	eagle_out=Sample_id+'.phased'

	os.system("bgzip -f "+Temp_dir+"/"+Sample_id+".final.vcf")
	os.system("tabix -p vcf "+Temp_dir+"/"+Sample_id+".final.vcf.gz")

	## run eagle
	os.system(RUN_HOME+"/eagle --vcfRef="+RUN_HOME+"/ref.50samples.phase3.20130502.genotypes.vcf.gz --vcfTarget="+Temp_dir+"/"+Sample_id+".final.vcf.gz"+" --geneticMapFile="+RUN_HOME+"/genetic_map_hg19.txt.gz --outPrefix="+Temp_dir+"/"+eagle_out+" 2>&1 | tee "+ Temp_dir+"/"+Sample_id+".eagle.log > /dev/null")
	
	os.system("gzip -fd "+Temp_dir+"/"+eagle_out+".vcf.gz")

	os.system("rm -f "+ Temp_dir+"/"+Sample_id+".final.vcf.gz")
	os.system("rm -f "+ Temp_dir+"/"+Sample_id+".final.vcf.gz.tbi")
	
	##==================================================================================================##
	## Step2. Calculate Ancestry algorithm for chromosome map 
	##
	## INPUT : phased vcf file after Beagle
	##
	## MID_OUTPUT : _Ancestry_hp1.txt / _Ancestry_hp2.txt
	##
	## OUTPUT : 
	##==================================================================================================##

	try:
		phasing_vcf=open(Temp_dir+"/"+eagle_out+".vcf", 'r')
	except:
		print 'aler^' + Report_num + '^' + Err_m[1]+" No eagle output" + '^alend;'
		#print "Can't open **phased** vcf file... please check the file"
		sys.exit()


	##=====================================================================================================##
	## DATABASE
	##
	## Reference dbSNP data (parsing data)
	##
	## Need requisitely dbsnp that is relevant to ancestry total snp
	## Must be check number of dbsnp and ancestry snp
	##=====================================================================================================##

	ref_dbsnp=open(RUN_HOME+"/RAW_661638_dbsnp_147.hg19.vcf", "r") 

	##=====================================================================================================##
	## Alfred database (parsing data-used major populations and snp)
	##=====================================================================================================##
	## Ref allele
	Ancestry_ref=open(RUN_HOME+"/GSA_POP24_alfred_parsing_ref.txt", "r") ## ref parsing file
	## Alt allele
	Ancestry_alt=open(RUN_HOME+"/GSA_POP24_alfred_parsing_alt.txt", "r") ## alt parsing file


	## Ancestry dictionary ---------------------------------------------------------------------------------##
	## alt_dic={RSID_Alt:{Pop:Freq}}

	alt_dic={}
	Ancestry_rsid={}
	Ancestry_pop = {}
	for line in Ancestry_alt:
		if not line.startswith("population"):
	 		ls=line.strip().split("\t")
			Pop, Size, RSID, alt, Freq = ls[0], ls[1], ls[2], ls[3], float(ls[4])
			Ancestry_rsid[RSID]=0
			Ancestry_pop[Pop] = 0
			KEY=RSID+":"+alt
			if KEY not in alt_dic:
				alt_dic[KEY]={Pop:Freq}			
			if Pop not in alt_dic[KEY]:
				alt_dic[KEY][Pop]=Freq
			alt_dic[KEY][Pop]=Freq
			

	## Dictionary of reference allele of Ancestry ----------------------------------------------------------##
	## ref_dic={RSID:{Pop:Freq}}

	ref_dic={}
	for line2 in Ancestry_ref:
		if not line2.startswith("population"):
			ls=line2.strip().split("\t")
			Pop, Size, RSID, Ref, Freq = ls[0], ls[1], ls[2], ls[3], float(ls[4].strip())
			Ancestry_rsid[RSID]=0
			KEY=RSID+":"+Ref
			if KEY not in ref_dic:
				ref_dic[KEY]={Pop:Freq}
			else:
				ref_dic[KEY][Pop]=Freq


	ref_population = sorted(Ancestry_pop.keys())  ### populations list of Ancestry(24)

	## Reference of Chromosome length ----------------------------------------------------------------------##

	reference_chr=open(RUN_HOME+"/Reference_LengthOfChromosome.txt","r")
	Chr_BINDIC={}
	win_reading_size = 1000000 #1M
	win_bin_size = 5000000 #5M
	for line in reference_chr:
		ls=line.strip().split("\t")
		Chr, Length =  ls[0], int(ls[2])
		Chr_BINDIC[Chr] = {}

		for i in range(0, Length, win_reading_size):
			start_point = i
			Chr_BINDIC[Chr][start_point] = 0


	##=====================================================================================================##
	## 1000genome af database 
	##=====================================================================================================##
	dat_1000g=open(RUN_HOME+"/af_GSA_std_cus.txt","r")

	g1000_marker={}
	af_alt_dic={}
	af_ref_dic={}
	for line in dat_1000g:
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			Chr, Pos, Ref, Alt, Rsid = ls[0], ls[1], ls[2], ls[3], ls[4]
			if Rsid != ".":
				EAS_f, SAS_f, EUR_f, AMR_f, AFR_f = float(ls[5]), float(ls[6]), float(ls[7]), float(ls[8]), float(ls[9])
				g1000_marker[Rsid]=0
			##=====================================
			## alt af
			##=====================================		
			key_alt=Rsid+":"+Alt

			if key_alt not in af_alt_dic:
				af_alt_dic[key_alt]={"EAS":EAS_f}			
			af_alt_dic[key_alt]["EAS"]=EAS_f

			if key_alt not in af_alt_dic:
				af_alt_dic[key_alt]={"SAS":SAS_f}			
			af_alt_dic[key_alt]["SAS"]=SAS_f

			if key_alt not in af_alt_dic:
				af_alt_dic[key_alt]={"EUR":EUR_f}			
			af_alt_dic[key_alt]["EUR"]=EUR_f

			if key_alt not in af_alt_dic:
				af_alt_dic[key_alt]={"AMR":AMR_f}			
			af_alt_dic[key_alt]["AMR"]=AMR_f

			if key_alt not in af_alt_dic:
				af_alt_dic[key_alt]={"AFR":AFR_f}			
			af_alt_dic[key_alt]["AFR"]=AFR_f

			##=====================================
			## ref af
			##=====================================
			key_ref=Rsid+":"+Ref

			if key_ref not in af_ref_dic:
				af_ref_dic[key_ref]={"EAS":1-EAS_f}			
			af_ref_dic[key_ref]["EAS"]=1-EAS_f

			if key_ref not in af_ref_dic:
				af_ref_dic[key_ref]={"SAS":1-SAS_f}			
			af_ref_dic[key_ref]["SAS"]=1-SAS_f

			if key_ref not in af_ref_dic:
				af_ref_dic[key_ref]={"EUR":1-EUR_f}			
			af_ref_dic[key_ref]["EUR"]=1-EUR_f

			if key_ref not in af_ref_dic:
				af_ref_dic[key_ref]={"AMR":1-AMR_f}			
			af_ref_dic[key_ref]["AMR"]=1-AMR_f

			if key_ref not in af_ref_dic:
				af_ref_dic[key_ref]={"AFR":1-AFR_f}			
			af_ref_dic[key_ref]["AFR"]=1-AFR_f

	dat_1000g.close()
	pop_1000=["EAS", "SAS", "EUR", "AMR", "AFR"]

	##=======================================================================================================##
	##
	## Algorism
	##
	##=======================================================================================================##

	##---------------------------
	## -- VCF file reading  --
	## Making vcf dictionary !!
	##---------------------------

	#print "start vcf calculating"
	vcf_a1_alt_dic={} #vcf_a1_alt_dic[Chr]={Loci:line_f}
	vcf_a1_ref_dic={}
	vcf_a2_alt_dic={}
	vcf_a2_ref_dic={}

	vcf_marker={}
	for line in phasing_vcf:
		if not line.startswith("#"):
			ls=line.split()
			Chr, Loci, vcf_rsid, Ref, Alt, Phasing_infor = "chr"+ls[0], int(ls[1]), ls[2], ls[3], ls[4], ls[9]
			#line_f=Chr+"\t"+Loci+"\t"+vcf_rsid+"\t"+Ref+"\t"+Alt+"\t"+Phasing_infor
			line_f=vcf_rsid+"\t"+Ref+"\t"+Alt

			vcf_marker[vcf_rsid]=0
			if not vcf_rsid == ".":
				Phasing_allele1=Phasing_infor.split("|")[0]
				Phasing_allele2=Phasing_infor.split("|")[1].split(":")[0]
				
				## Allele 1 : ---------------------------------------------##
				if Phasing_allele1 == "1": 
					if Chr not in vcf_a1_alt_dic:
						vcf_a1_alt_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a1_alt_dic[Chr]:
						vcf_a1_alt_dic[Chr][Loci]=line_f
					vcf_a1_alt_dic[Chr][Loci]=line_f
				
				if Phasing_allele1 =="0":
					if Chr not in vcf_a1_ref_dic:
						vcf_a1_ref_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a1_ref_dic[Chr]:
						vcf_a1_ref_dic[Chr][Loci]=line_f
					vcf_a1_ref_dic[Chr][Loci]=line_f
				
				## Allele 2 : ---------------------------------------------##
				if Phasing_allele2 == "1": 
					if Chr not in vcf_a2_alt_dic:
						vcf_a2_alt_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a2_alt_dic[Chr]:
						vcf_a2_alt_dic[Chr][Loci]=line_f
					vcf_a2_alt_dic[Chr][Loci]=line_f
					
				if Phasing_allele2 =="0":
					if Chr not in vcf_a2_ref_dic:
						vcf_a2_ref_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a2_ref_dic[Chr]:
						vcf_a2_ref_dic[Chr][Loci]=line_f
					vcf_a2_ref_dic[Chr][Loci]=line_f

	#print vcf_a1_alt_dic.keys()

	##---------------------------------------------------------------------------------------------------##
	## No marker in vcf --> ref allele 
	##---------------------------------------------------------------------------------------------------##
	ref_key={}
	for key in Ancestry_rsid:
		if key in vcf_marker:
			pass
		else:
			ref_key[key]=0

	ref2_key={}
	for key in g1000_marker:
		if key in vcf_marker:
			pass
		else:
			ref2_key[key]=0

	##---------------------------------------------------------------------------------------------------##
	## Need ref vcf file (dbSNP)
	##---------------------------------------------------------------------------------------------------##

	dbSNP_dic={}
	for ref_vcf in ref_dbsnp:
		ls=ref_vcf.split("\t")
		Chr, Loci, ref_rsid, Ref = "chr"+ls[0], int(ls[1]), ls[2], ls[3]
		line_f=ref_rsid+"\t"+Ref+"\t"+Ref

		if ref_rsid in ref_key:
			if Chr not in dbSNP_dic:
				dbSNP_dic[Chr]={Loci:line_f}
			if Loci not in dbSNP_dic[Chr]:
				dbSNP_dic[Chr][Loci]=line_f
			dbSNP_dic[Chr][Loci]=line_f

	#print "len dbsnp", len(dbSNP_dic)

	dat_1000g=open(RUN_HOME+"/af_GSA_std_cus.txt","r")
	g1000_ref_dic={}
	for line in dat_1000g:
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			Chr, Loci, ref_rsid, Ref = ls[0], int(ls[1]), ls[4], ls[2]
			line_f=ref_rsid+"\t"+Ref+"\t"+Ref

			if ref_rsid in ref2_key:
				if Chr not in g1000_ref_dic:
					g1000_ref_dic[Chr]={Loci:line_f}
				if Loci not in g1000_ref_dic[Chr]:
					g1000_ref_dic[Chr][Loci]=line_f
				g1000_ref_dic[Chr][Loci]=line_f

	dat_1000g.close()
	##------------------------------------------------------------------------------------------------##
	## Calculate window reading
	##------------------------------------------------------------------------------------------------##
	Chromosome={}

	POP_dic={} ### Allele1_Final result of algorism (Sumation of frequency per chr bin)
	POP2_dic={} ### Allele2_Final result of algorism (Sumation of frequency per chr bin)
	sub_dic={} ### alt_dic[rsid:allele]={Pop:Freq}
	sub2_dic={} ### ref_dic[rsid:seq]={Pop:Freq}
	sub3_dic={}
	count1_dic={}
	count2_dic={}

	POP_1000={} 
	POP2_1000={} 
	sub_1000={} 
	sub2_1000={} 
	sub3_1000={}
	#ref_1000={}
	count1_1000={}
	count2_1000={}

	Bin_dic={}
	CHR_LIST=[]
	for i in range(1,23):
		CHR_LIST.append("chr"+str(i))
	#print CHR_LIST

	for Chr_infor in natural_sort(CHR_LIST):
		#print "===", Chr_infor, "==="
		#if Chr_infor == "chr22":
		Chromosome[Chr_infor]=0
		POP_dic[Chr_infor] = {}
		POP2_dic[Chr_infor] = {}
		Bin_dic[Chr_infor] = {}
		count1_dic[Chr_infor] = {}
		count2_dic[Chr_infor] = {}

		for ethnicity in ref_population:
			POP_dic[Chr_infor][ethnicity] = {}
			POP2_dic[Chr_infor][ethnicity] = {}
			count1_dic[Chr_infor][ethnicity] = {}
			count2_dic[Chr_infor][ethnicity] = {}


		##----------------------------------------------------------------------------
		## Calculating Afred database !!!
		##----------------------------------------------------------------------------
		## Allele 1 ------------------------------------------------------------------
		if Chr_infor in vcf_a1_alt_dic:
			for j in vcf_a1_alt_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):
						vcf_rsid = vcf_a1_alt_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a1_alt_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a1_alt_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Alt
						if key in alt_dic:
							sub_dic = alt_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub_dic.keys():
								if bin_val not in POP_dic[Chr_infor][ethnic]:
									POP_dic[Chr_infor][ethnic][bin_val] = 0
								POP_dic[Chr_infor][ethnic][bin_val] += sub_dic[ethnic]

								if bin_val not in count1_dic[Chr_infor][ethnic]:
									count1_dic[Chr_infor][ethnic][bin_val] = 0
								count1_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		if Chr_infor in vcf_a1_ref_dic:
			for j in vcf_a1_ref_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a1_ref_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a1_ref_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a1_ref_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Ref
						if key in ref_dic:
							sub_dic = ref_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub_dic.keys():
								if bin_val not in POP_dic[Chr_infor][ethnic]:
									POP_dic[Chr_infor][ethnic][bin_val] = 0
								POP_dic[Chr_infor][ethnic][bin_val] += sub_dic[ethnic]

								if bin_val not in count1_dic[Chr_infor][ethnic]:
									count1_dic[Chr_infor][ethnic][bin_val] = 0
								count1_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		## Allele 2 ------------------------------------------------------------------
		if Chr_infor in vcf_a2_alt_dic:
			for j in vcf_a2_alt_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a2_alt_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a2_alt_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a2_alt_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Alt
						if key in alt_dic:
							sub2_dic = alt_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub2_dic.keys():
								if bin_val not in POP2_dic[Chr_infor][ethnic]:
									POP2_dic[Chr_infor][ethnic][bin_val] = 0
								POP2_dic[Chr_infor][ethnic][bin_val] += sub2_dic[ethnic]

								if bin_val not in count2_dic[Chr_infor][ethnic]:
									count2_dic[Chr_infor][ethnic][bin_val] = 0
								count2_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		if Chr_infor in vcf_a2_ref_dic:
			for j in vcf_a2_ref_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a2_ref_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a2_ref_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a2_ref_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Ref
						if key in ref_dic:
							sub2_dic = ref_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub2_dic.keys():
								if bin_val not in POP2_dic[Chr_infor][ethnic]:
									POP2_dic[Chr_infor][ethnic][bin_val] = 0
								POP2_dic[Chr_infor][ethnic][bin_val] += sub2_dic[ethnic]

								if bin_val not in count2_dic[Chr_infor][ethnic]:
									count2_dic[Chr_infor][ethnic][bin_val] = 0
								count2_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		## No vcf (Ref) ------------------------------------------------------------------
		if Chr_infor in dbSNP_dic:
			for j in dbSNP_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = dbSNP_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= dbSNP_dic[Chr_infor][j].split("\t")[1]
						#vcf_Ref = dbSNP_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Ref
						if key in ref_dic:
							sub3_dic = ref_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub3_dic.keys():
								if bin_val not in POP_dic[Chr_infor][ethnic]:
									POP_dic[Chr_infor][ethnic][bin_val] = 0
								POP_dic[Chr_infor][ethnic][bin_val] += sub3_dic[ethnic]

								if bin_val not in count1_dic[Chr_infor][ethnic]:
									count1_dic[Chr_infor][ethnic][bin_val] = 0
								count1_dic[Chr_infor][ethnic][bin_val] += 1

							for ethnic in sub3_dic.keys():
								if bin_val not in POP2_dic[Chr_infor][ethnic]:
									POP2_dic[Chr_infor][ethnic][bin_val] = 0
								POP2_dic[Chr_infor][ethnic][bin_val] += sub3_dic[ethnic]

								if bin_val not in count2_dic[Chr_infor][ethnic]:
									count2_dic[Chr_infor][ethnic][bin_val] = 0
								count2_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0
		
		##----------------------------------------------------------------------------
		## Calculating 1000 genome database !!!
		##----------------------------------------------------------------------------
		POP_1000[Chr_infor] = {}
		POP2_1000[Chr_infor] = {}
		count1_1000[Chr_infor] = {}
		count2_1000[Chr_infor] = {}
		#Bin_dic[Chr_infor] = {}
		for ethnicity in pop_1000:
			POP_1000[Chr_infor][ethnicity] = {}
			POP2_1000[Chr_infor][ethnicity] = {}
			count1_1000[Chr_infor][ethnicity] = {}
			count2_1000[Chr_infor][ethnicity] = {}

		## Allele 1 ------------------------------------------------------------------
		if Chr_infor in vcf_a1_alt_dic:
			for j in vcf_a1_alt_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a1_alt_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a1_alt_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a1_alt_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Alt
						if key in af_alt_dic:
							sub_1000 = af_alt_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub_1000.keys():
								if bin_val not in POP_1000[Chr_infor][ethnic]:
									POP_1000[Chr_infor][ethnic][bin_val] = 0
								POP_1000[Chr_infor][ethnic][bin_val] += sub_1000[ethnic]

								if bin_val not in count1_1000[Chr_infor][ethnic]:
									count1_1000[Chr_infor][ethnic][bin_val] = 0
								count1_1000[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		if Chr_infor in vcf_a1_ref_dic:
			for j in vcf_a1_ref_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a1_ref_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a1_ref_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a1_ref_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Ref
						if key in af_ref_dic:
							sub_1000 = af_ref_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub_1000.keys():
								if bin_val not in POP_1000[Chr_infor][ethnic]:
									POP_1000[Chr_infor][ethnic][bin_val] = 0
								POP_1000[Chr_infor][ethnic][bin_val] += sub_1000[ethnic]
							
								if bin_val not in count1_1000[Chr_infor][ethnic]:
									count1_1000[Chr_infor][ethnic][bin_val] = 0
								count1_1000[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		## Allele 2 ------------------------------------------------------------------
		if Chr_infor in vcf_a2_alt_dic:
			for j in vcf_a2_alt_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a2_alt_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a2_alt_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a2_alt_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Alt
						if key in af_alt_dic:
							sub2_1000 = af_alt_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub2_1000.keys():
								if bin_val not in POP2_1000[Chr_infor][ethnic]:
									POP2_1000[Chr_infor][ethnic][bin_val] = 0
								POP2_1000[Chr_infor][ethnic][bin_val] += sub2_1000[ethnic]

								if bin_val not in count2_1000[Chr_infor][ethnic]:
									count2_1000[Chr_infor][ethnic][bin_val] = 0
								count2_1000[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		if Chr_infor in vcf_a2_ref_dic:
			for j in vcf_a2_ref_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a2_ref_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a2_ref_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a2_ref_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Ref
						if key in af_ref_dic:
							sub2_1000 = af_ref_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub2_1000.keys():
								if bin_val not in POP2_1000[Chr_infor][ethnic]:
									POP2_1000[Chr_infor][ethnic][bin_val] = 0
								POP2_1000[Chr_infor][ethnic][bin_val] += sub2_1000[ethnic]

								if bin_val not in count2_1000[Chr_infor][ethnic]:
									count2_1000[Chr_infor][ethnic][bin_val] = 0
								count2_1000[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0


		## No vcf (Ref) ------------------------------------------------------------------
		
		if Chr_infor in g1000_ref_dic: 
			for j in g1000_ref_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					#if bin_val <= j and j <= bin_val+win_bin_size:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = g1000_ref_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= g1000_ref_dic[Chr_infor][j].split("\t")[1]
						#vcf_Ref = af_ref_dic[Chr_infor][j].split("\t")[2]
						key=vcf_rsid+":"+vcf_Ref

						if key in af_ref_dic:
							sub3_1000 = af_ref_dic[key] #af_ref_dic[rsid:allele]={Pop:Freq}
							for ethnic in sub3_1000.keys():
								if bin_val not in POP_1000[Chr_infor][ethnic]:
									POP_1000[Chr_infor][ethnic][bin_val] = 0
								POP_1000[Chr_infor][ethnic][bin_val] += sub3_1000[ethnic]

								if bin_val not in count1_1000[Chr_infor][ethnic]:
									count1_1000[Chr_infor][ethnic][bin_val] = 0
								count1_1000[Chr_infor][ethnic][bin_val] += 1

							for ethnic in sub3_1000.keys():
								if bin_val not in POP2_1000[Chr_infor][ethnic]:
									POP2_1000[Chr_infor][ethnic][bin_val] = 0
								POP2_1000[Chr_infor][ethnic][bin_val] += sub3_1000[ethnic]

								if bin_val not in count2_1000[Chr_infor][ethnic]:
									count2_1000[Chr_infor][ethnic][bin_val] = 0
								count2_1000[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

	#print "done... calculate (window reading)"

	##===============================================================================================================##
	##
	## Calculate final pop
	##
	##===============================================================================================================##

	mid_out1=open(Output_dir+'/'+Sample_id+'_Ancestry_hp1.txt', "w")
	mid_out2=open(Output_dir+'/'+Sample_id+'_Ancestry_hp2.txt', "w")
	#mid_out1.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(pop_1000)+'\tDesition1000\tDesition_alfred'+"\n")
	#mid_out2.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(pop_1000)+'\tDesition1000\tDesition_alfred'+"\n")
	mid_out1.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred'+"\n")
	mid_out2.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred'+"\n")

	final_out = open(Output_dir+'/'+Sample_id+'_FinalPOP.txt','w')
	#final_out.write('#Sample_VCF:'+str(eagle_out)+'\n'+'#Report_number:'+str(Report_num)+'\n'+'#User_id:'+str(User_id)+'\n'+'#Chr\tBin_range\tPopulation\n')

	##--------------------------
	## Reference
	##--------------------------

	Desition_dic={} #{1:pop, 2: pop, 3: pop ...}
	for index in range(len(ref_population)):
		Desition_dic[index]=ref_population[index]

	#print Desition_dic.keys()

	Desition_1000={}
	for index in range(len(pop_1000)):
		Desition_1000[index]=pop_1000[index]


	##==========================================================================================================
	## Allele 1
	##==========================================================================================================

	##result_pop=[]
	modi_pop1=[]

	for Chr in natural_sort(Chromosome.keys()):
		if Chr in Bin_dic:
			m_bin = sorted(Bin_dic[Chr])
			for posBin in m_bin:
				m_binFreq = []
				m_binFreq2=[]
				#final_POP = []
				Count1=[]

				binFreq1000=[]
				binFreq1000_2=[]
				final_1000POP=[]
				Count1_1000=[]
				
				## alfred pop
				for popName in ref_population:
					#print popName
					try:m_binFreq.append(float('%0.4f' %(POP_dic[Chr][popName][posBin])))
					except:m_binFreq.append(float('0'))
					try:m_binFreq2.append('%0.4f' %(POP_dic[Chr][popName][posBin]))
					except:m_binFreq2.append('0')
					try:Count1.append(int(count1_dic[Chr][popName][posBin]))
					except:Count1.append('0')

				## 1000 pop
				for pop_name in pop_1000:
					try:binFreq1000.append(float('%0.4f' %(POP_1000[Chr][pop_name][posBin]))) 
					except:binFreq1000.append(float('0'))
					try:binFreq1000_2.append('%0.4f' %(POP_1000[Chr][pop_name][posBin]))
					except:binFreq1000_2.append('0')
					try:Count1_1000.append(int(count1_1000[Chr][pop_name][posBin]))
					except:Count1_1000.append('0')


				if sum(m_binFreq) != 0 and len(m_binFreq) == 24 :

					final_count1 = max(Count1) 
					final_count = max(Count1_1000)

					final_POP = []

					## Final population
					if final_count > 10 and final_count1 > 10:

						final_index1000 = binFreq1000.index(max(binFreq1000)) ## 1000 pop
						final_1000POP.append(Desition_1000[final_index1000])

						if final_1000POP == ['AFR']: 
							final_index1 = m_binFreq[0:3].index(max(m_binFreq[0:3])) ## alfred pop
							final_POP.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])
							#result_pop.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])

						if final_1000POP == ['SAS']: #South Asian
							final_index2 = m_binFreq[3:6].index(max(m_binFreq[3:6])) ## alfred pop
							final_POP.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])
							#result_pop.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])

						if final_1000POP == ['EAS']: 
							final_index3 = m_binFreq[6:9].index(max(m_binFreq[6:9])) ## alfred pop
							#final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
							###result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
						
							Han=float(m_binFreq[6])
							Japan=float(m_binFreq[7])
							Korean=float(m_binFreq[8])

							if ''.join(final_POP) == "EastAsia:Japanese" and abs(Korean-Japan) < 0.1: # and abs(Korean-Han) < 0.2:
								final_POP.append("EastAsia:Koreans")
								#result_pop.append("EastAsia:Koreans")
							elif ''.join(final_POP) == "EastAsia:Han":
								final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

							else:
								final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

						if final_1000POP == ['EUR']: 
							#final_index4 = (m_binFreq[9:11]+[m_binFreq[13]]+m_binFreq[20:]).index(max(m_binFreq[9:11]+[m_binFreq[13]]+m_binFreq[20:])) ## alfred pop
							#final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+[Desition_dic.keys()[13]]+Desition_dic.keys()[20:])[final_index4]])
							final_index4 = (m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:]).index(max(m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:])) ## Oceania, Siberia
							final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])

							#result_pop.append(Desition_dic[(Desition_dic.keys()[9:11]+[Desition_dic.keys()[13]]+Desition_dic.keys()[20:])[final_index4]])

						
						if final_1000POP == ['AMR']: 
							final_index5 = (m_binFreq[11:13]+m_binFreq[17:20]).index(max(m_binFreq[11:13]+m_binFreq[17:20])) ## alfred pop
							final_POP.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])
							#result_pop.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])

					else:
						final_POP.append("NA")
						final_1000POP.append("NA")
				
					modi_pop1.append(''.join(final_1000POP))	
					mid_out1.write(Chr+"\t"+str(posBin)+"\t"+str(final_count)+"\t"+str(final_count1)+"\t"+"\t".join(m_binFreq2)+"\t"+''.join(final_1000POP)+'\t'+''.join(final_POP)+'\n')

	mid_out1.close()


	#--------------------------------------------------------------------------------------------
	# modi popultations - Allele1 (Updat. 17.06.15)
	#--------------------------------------------------------------------------------------------
	pop_lenth=len(modi_pop1)
	Final_modiPOP1=[]
	for i, k in zip(range(0,pop_lenth+1, 10), range(0,pop_lenth+1, 10)[1:]+[len(modi_pop1)]):
		#print i,k, modi_pop1[i:k]

		if "NA" in modi_pop1[i:k]:
			Final_modiPOP1.extend(modi_pop1[i:k])

		else:
			if len(set(modi_pop1[i:k]))==1:
				Final_modiPOP1.extend(modi_pop1[i:k])
			elif 1 <= len(set(modi_pop1[i:k])) <= 2:
				modi_key=dict((x, modi_pop1[i:k].count(x)) for x in sorted(list(set(modi_pop1[i:k])))).keys()
				modi_list=dict((x, modi_pop1[i:k].count(x)) for x in sorted(list(set(modi_pop1[i:k])))).values()
				major_index=modi_list.index(max(modi_list))
				major_pop=modi_key[major_index]

				Final_modiPOP1.extend([major_pop]*10)

			else:
				Final_modiPOP1.extend(modi_pop1[i:k])

	#print len(Final_modiPOP1)


	# re-output-------------------------------------------------------------------------------------------
	mid_out1_modi=open(Output_dir+'/'+Sample_id+'_Ancestry_hp1_modi.txt',"w")
	mid_out1_modi.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred\tModi1000'+"\n")

	modi_count1=0
	for i, line in zip(range(0,len(Final_modiPOP1)+1), open(Output_dir+'/'+Sample_id+'_Ancestry_hp1.txt',"r")):
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			mid_out1_modi.write("\t".join(ls)+"\t"+Final_modiPOP1[i-1]+"\n")

	mid_out1_modi.close()

	# moid pop2-------------------------------------------------------------------------------------------
	mid_out1_modi_final=open(Output_dir+'/'+Sample_id+'_Ancestry_hp1_modi_final.txt',"w")
	mid_out1_modi_final.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred\tModi1000\tModiPOP'+"\n")
	result_pop=[]
	for line in open(Output_dir+'/'+Sample_id+'_Ancestry_hp1_modi.txt',"r"):
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			Final_modiPOP = ls[-1]
			pop_ratio=ls[4:28]

			final_POP=[]

			if Final_modiPOP == 'AFR': 
				final_index1 = pop_ratio[0:3].index(max(pop_ratio[0:3])) ## alfred pop
				final_POP.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])
				result_pop.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])

			elif Final_modiPOP == 'SAS': #South Asian
				final_index2 = pop_ratio[3:6].index(max(pop_ratio[3:6])) ## alfred pop
				final_POP.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])
				result_pop.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])

			elif Final_modiPOP == 'EAS': 
				final_index3 = pop_ratio[6:9].index(max(pop_ratio[6:9])) ## alfred pop
				final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
				#result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
			
				Han=float(pop_ratio[6])
				Japan=float(pop_ratio[7])
				Korean=float(pop_ratio[8])

				if ''.join(final_POP) == "EastAsia:Japanese" and abs(Korean-Japan) < 0.1: # and abs(Korean-Han) < 0.2:
					final_POP=["EastAsia:Koreans"]
					result_pop.append("EastAsia:Koreans")

				elif ''.join(final_POP) == "EastAsia:Han":
					final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]
					result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

				else:
					final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]
					result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

			elif Final_modiPOP == 'EUR': 
				final_index4 = (pop_ratio[9:11]+pop_ratio[13:17]+pop_ratio[20:]).index(max(pop_ratio[9:11]+pop_ratio[13:17]+pop_ratio[20:])) ## alfred pop
				final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
				result_pop.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
			
			elif Final_modiPOP == 'AMR': 
				final_index5 = (pop_ratio[11:13]+pop_ratio[17:20]).index(max(pop_ratio[11:13]+pop_ratio[17:20])) ## alfred pop
				final_POP.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])
				result_pop.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])

			else:
				final_POP.append("NA")

			mid_out1_modi_final.write(line.strip()+"\t"+''.join(final_POP)+'\n')

	mid_out1_modi_final.close()



	##==========================================================================================================
	## Allele 2
	##==========================================================================================================

	modi_pop2=[]

	for Chr in natural_sort(Chromosome.keys()):
		if Chr in Bin_dic:
			m_bin = sorted(Bin_dic[Chr])

			for posBin in m_bin:
				m_binFreq = []
				m_binFreq2=[]
				#final_POP = []

				Count2=[]

				binFreq1000 = []
				binFreq1000_2=[]
				final_1000POP = []
				Count2_1000=[]

				## alfred pop
				for popName in ref_population:
					try:m_binFreq.append(float('%0.4f' %(POP2_dic[Chr][popName][posBin])))
					except:m_binFreq.append(float('0'))
					try:m_binFreq2.append('%0.4f' %(POP2_dic[Chr][popName][posBin]))
					except:m_binFreq2.append('0')
					try:Count2.append(int(count2_dic[Chr][popName][posBin]))
					except:Count2.append('0')

				## 1000 pop	
				for pop_name in pop_1000:
					try:binFreq1000.append(float('%0.4f' %(POP2_1000[Chr][pop_name][posBin]))) 
					except:binFreq1000.append(float('0'))
					try:binFreq1000_2.append('%0.4f' %(POP2_1000[Chr][pop_name][posBin]))
					except:binFreq1000_2.append('0')
					try:Count2_1000.append(int(count2_1000[Chr][pop_name][posBin]))
					except:Count2_1000.append('0')


				if sum(m_binFreq) != 0 and len(m_binFreq) ==24 :

					final_count1 = max(Count2) 
					final_count = max(Count2_1000)

					final_POP = []

					if final_count > 10 and final_count1 > 10:

						final_index1000 = binFreq1000.index(max(binFreq1000)) ## 1000 pop
						final_1000POP.append(Desition_1000[final_index1000])

						## Final population
						if final_1000POP == ['AFR']: 
							final_index1 = m_binFreq[0:3].index(max(m_binFreq[0:3])) ## alfred pop
							final_POP.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])
							#result_pop.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])

						if final_1000POP == ['SAS']: #South Asian
							final_index2 = m_binFreq[3:6].index(max(m_binFreq[3:6])) ## alfred pop
							final_POP.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])
							#result_pop.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])

						if final_1000POP == ['EAS']: 
							final_index3 = m_binFreq[6:9].index(max(m_binFreq[6:9])) ## alfred pop
							final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
							#result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

							Han=float(m_binFreq[6])
							Japan=float(m_binFreq[7])
							Korean=float(m_binFreq[8])

							if ''.join(final_POP) == "EastAsia:Japanese" and abs(Korean-Japan) < 0.1: # and abs(Korean-Han) < 0.2:
								final_POP=["EastAsia:Koreans"]
								#result_pop.append("EastAsia:Koreans")
							elif ''.join(final_POP) == "EastAsia:Han":
								final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]

							else:
								final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]

						if final_1000POP == ['EUR']: 
							#final_index4 = (m_binFreq[9:11]+[m_binFreq[13]]+m_binFreq[20:]).index(max(m_binFreq[9:11]+[m_binFreq[13]]+m_binFreq[20:])) ## alfred pop
							#final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+[Desition_dic.keys()[13]]+Desition_dic.keys()[20:])[final_index4]])
							final_index4 = (m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:]).index(max(m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:])) ## Oceania, Siberia
							final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
							#result_pop.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
						
						if final_1000POP == ['AMR']: 
							final_index5 = (m_binFreq[11:13]+m_binFreq[17:20]).index(max(m_binFreq[11:13]+m_binFreq[17:20])) ## alfred pop
							final_POP.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])
							#result_pop.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])
					else:
						final_POP.append("NA")
						final_1000POP.append("NA")

					modi_pop2.append(''.join(final_1000POP))
					mid_out2.write(Chr+"\t"+str(posBin)+"\t"+str(final_count)+"\t"+str(final_count1)+"\t"+"\t".join(m_binFreq2)+"\t"+''.join(final_1000POP)+'\t'+''.join(final_POP)+'\n')
				
	mid_out2.close()

	#--------------------------------------------------------------------------------------------
	# modi popultations - Allele2 (Updat. 17.06.15)
	#--------------------------------------------------------------------------------------------
	pop_lenth=len(modi_pop2)
	Final_modiPOP2=[]
	for i, k in zip(range(0,pop_lenth+1, 10), range(0,pop_lenth+1, 10)[1:]+[len(modi_pop2)]):
		#print i,k, modi_pop2[i:k]

		if "NA" in modi_pop2[i:k]:
			Final_modiPOP2.extend(modi_pop2[i:k])

		else:
			if len(set(modi_pop2[i:k]))==1:
				Final_modiPOP2.extend(modi_pop2[i:k])

			elif 1 <= len(set(modi_pop2[i:k])) <= 2:
				modi_key=dict((x, modi_pop2[i:k].count(x)) for x in sorted(list(set(modi_pop2[i:k])))).keys()
				modi_list=dict((x, modi_pop2[i:k].count(x)) for x in sorted(list(set(modi_pop2[i:k])))).values()
				major_index=modi_list.index(max(modi_list))
				major_pop=modi_key[major_index]

				Final_modiPOP2.extend([major_pop]*10)

			else:
				Final_modiPOP2.extend(modi_pop2[i:k])



	# re-output
	mid_out2_modi=open(Output_dir+'/'+Sample_id+'_Ancestry_hp2_modi.txt',"w")
	mid_out2_modi.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tModi1000\tDesition_alfred'+"\n")

	for i, line in zip(range(0,len(Final_modiPOP2)+1), open(Output_dir+'/'+Sample_id+'_Ancestry_hp2.txt',"r")):
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			mid_out2_modi.write("\t".join(ls)+"\t"+Final_modiPOP2[i-1]+"\n")

	mid_out2_modi.close()

	# moid pop2
	mid_out2_modi_final=open(Output_dir+'/'+Sample_id+'_Ancestry_hp2_modi_final.txt',"w")
	mid_out2_modi_final.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred\tModi1000\tModiPOP'+"\n")

	for line in open(Output_dir+'/'+Sample_id+'_Ancestry_hp2_modi.txt',"r"):
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			Final_modiPOP = ls[-1]
			pop_ratio=ls[4:28]

			final_POP=[]

			if Final_modiPOP == 'AFR': 
				final_index1 = pop_ratio[0:3].index(max(pop_ratio[0:3])) ## alfred pop
				final_POP.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])
				result_pop.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])

			elif Final_modiPOP == 'SAS': #South Asian
				final_index2 = pop_ratio[3:6].index(max(pop_ratio[3:6])) ## alfred pop
				final_POP.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])
				result_pop.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])

			elif Final_modiPOP == 'EAS': 
				final_index3 = pop_ratio[6:9].index(max(pop_ratio[6:9])) ## alfred pop
				final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
				#result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
			
				Han=float(pop_ratio[6])
				Japan=float(pop_ratio[7])
				Korean=float(pop_ratio[8])

				if ''.join(final_POP) == "EastAsia:Japanese" and abs(Korean-Japan) < 0.1: # and abs(Korean-Han) < 0.2:
					final_POP=["EastAsia:Koreans"]
					result_pop.append("EastAsia:Koreans")
				
				elif ''.join(final_POP) == "EastAsia:Han":
					final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]
					result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

				else:
					final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]
					result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

			elif Final_modiPOP == 'EUR': 
				final_index4 = (pop_ratio[9:11]+pop_ratio[13:17]+pop_ratio[20:]).index(max(pop_ratio[9:11]+pop_ratio[13:17]+pop_ratio[20:])) ## alfred pop
				final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
				result_pop.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
			
			elif Final_modiPOP == 'AMR': 
				final_index5 = (pop_ratio[11:13]+pop_ratio[17:20]).index(max(pop_ratio[11:13]+pop_ratio[17:20])) ## alfred pop
				final_POP.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])
				result_pop.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])

			else:
				final_POP.append("NA")

			mid_out2_modi_final.write(line.strip()+'\t'+''.join(final_POP)+'\n')

	mid_out2_modi_final.close()



	##=============================================================================================================
	##
	## Statistics of populations
	##
	##=============================================================================================================

	##print '\n** Ancestry algorithm was done...\n'
	##print '\n** Results are'

	out = open(Output_dir+'/'+Sample_id+'_AncestryResult.txt', "w")
	out.write('#Version3.4.1\n')
	out.write('#Sample_VCF:'+Sample_id+'\n'+'#Report_number:'+Report_num+'\n'+'#User_id:'+User_id+'\n'+'#Geographic_region\tPopulation\tPercentage(%)\n')

	## hp1
	decision_pop={}
	for element in result_pop:
		decision_pop[element]=0

	out_dic={}
	for list_val in decision_pop:
		list_count=[]
		list_count.append(result_pop.count(list_val))
		out_dic[list_val]= (float(list_count[0])/float(len(result_pop)))*100

	out_list=[]
	for key, value in sorted(out_dic.iteritems(), reverse=True, key=lambda (k,v): (v,k)):
		out_list.append("%s: %s" %(key, value))

	for out_li in out_list:
		out_col = out_li.split(":")
		out.write('\t'.join(out_col)+'\n')

	out.close()
	##======================================================================================================================
	##
	## Convert output form fo chr map 
	##
	##======================================================================================================================
	#print 'Making **Final POP** output file...'

	## read mid_out files
	HP1_result=open(Output_dir+'/'+Sample_id+'_Ancestry_hp1_modi_final.txt', 'r')
	HP2_result=open(Output_dir+'/'+Sample_id+'_Ancestry_hp2_modi_final.txt', 'r')

	#f1=hp1_result.readlines()
	#new_list = "chr1:start:end:europe"
	f1_read = HP1_result.readline()
	new1_list = []
	while(f1_read):
		if not f1_read.startswith("#"):
			new1_line = []
			if len(new1_list) != 0:	
				if new1_list[-1].strip().split(":")[-1] == f1_read.strip().split("\t")[-2].split(":")[0] and new1_list[-1].strip().split(":")[0] == f1_read.strip().split("\t")[0]:
					new1_line.append(new1_list[-1].strip().split(":")[0])
					new1_line.append(new1_list[-1].strip().split(":")[1])
					new1_line.append(str(int(f1_read.strip().split("\t")[1])+win_reading_size))
					new1_line.append(new1_list[-1].strip().split(":")[3])
					new1_list.pop()
				else:
					new1_line.append(f1_read.strip().split("\t")[0])
					new1_line.append(f1_read.strip().split("\t")[1])
					new1_line.append(str(int(f1_read.strip().split("\t")[1])+win_reading_size))
					new1_line.append(f1_read.strip().split("\t")[-2].split(":")[0])
				new1_list.append(":".join(new1_line))
			else:
				new1_line.append(f1_read.strip().split("\t")[0])
				new1_line.append(f1_read.strip().split("\t")[1])
				new1_line.append(str(int(f1_read.strip().split("\t")[1])+win_reading_size))
				new1_line.append(f1_read.strip().split("\t")[-2].split(":")[0])
				new1_list.append(":".join(new1_line))
				
		f1_read = HP1_result.readline()


	f2_read = HP2_result.readline()
	new2_list = []
	while(f2_read):
		if not f2_read.startswith("#"):
			new2_line = []
			if len(new2_list) != 0:	
				if new2_list[-1].strip().split(":")[-1] == f2_read.strip().split("\t")[-2].split(":")[0] and new2_list[-1].strip().split(":")[0] == f2_read.strip().split("\t")[0]:
					new2_line.append(new2_list[-1].strip().split(":")[0])
					new2_line.append(new2_list[-1].strip().split(":")[1])
					new2_line.append(str(int(f2_read.strip().split("\t")[1])+win_reading_size))
					new2_line.append(new2_list[-1].strip().split(":")[3])
					new2_list.pop()
				else:
					new2_line.append(f2_read.strip().split("\t")[0])
					new2_line.append(f2_read.strip().split("\t")[1])
					new2_line.append(str(int(f2_read.strip().split("\t")[1])+win_reading_size))
					new2_line.append(f2_read.split("\t")[-2].split(":")[0])
				new2_list.append(":".join(new2_line))
			else:
				new2_line.append(f2_read.strip().split("\t")[0])
				new2_line.append(f2_read.strip().split("\t")[1])
				new2_line.append(str(int(f2_read.strip().split("\t")[1])+win_reading_size))
				new2_line.append(f2_read.strip().split("\t")[-2].split(":")[0])
				new2_list.append(":".join(new2_line))
				
		f2_read = HP2_result.readline()

	for result1 in new1_list:
		ls=result1.split(":")
		Chr, Bin_start, Bin_end, Pop = ls[0:]
		final_out.write(Chr +'_1'+ '\t'+ Bin_start +'-'+ Bin_end +'\t'+ Pop.strip().upper() +'\n')

	for result2 in new2_list:
		ls=result2.split(":")
		Chr, Bin_start, Bin_end, Pop = ls[0:]
		final_out.write(Chr +'_2'+ '\t'+ Bin_start +'-'+ Bin_end +'\t'+ Pop.strip().upper() +'\n')

	final_out.close()

	os.system("sort -k1V "+ Output_dir+"/"+Sample_id+"_FinalPOP.txt > "+Output_dir+"/sorted_"+Sample_id+"_FinalPOP.txt")

	
	os.system("rm -f "+Output_dir+"/"+Sample_id+"_FinalPOP.txt")
	os.system("rm -f "+Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf")
	os.system("rm -f "+Temp_dir+"/"+eagle_out)
	os.system("rm -f "+Temp_dir+"/"+Sample_id+".eagle.log")
	os.system("rm -f "+Temp_dir+"/"+Sample_id+".final.vcf")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp1.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp2.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp1_modi.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp2_modi.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp1_modi_final.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp2_modi_final.txt")


	os.system("rm -f "+Temp_dir+"/eagle."+Sample_id+".vcf.gz")
	os.system("rm -f "+Temp_dir+"/eagle."+Sample_id+".vcf.gz.tbi")

	os.system("rm -f "+Temp_dir+"/"+eagle_out+".vcf")
	
	os.system("cat "+Output_dir+"/"+Sample_id+"_AncestryResult.txt > "+Output_dir+"/"+Report_num+".txt")
	os.system("echo '------------------------------------------' >> "+Output_dir+"/"+Report_num+".txt")
	os.system("cat "+Output_dir+"/sorted_"+Sample_id+'_FinalPOP.txt'+" >> "+Output_dir+"/"+Report_num+".txt")

	print "alrp^" + Report_num + "^" + Output_dir+'/'+Report_num+".txt"+"^alend;"
	print "aled^"+ Report_num +"^end^alend;"


except Exception as er:
	if Report_num is not None:
		print 'aler^' + Report_num + '^' + Err_m[1]+" "+`er` + '^alend;'
	else:
		print 'aler^' + 'None' + '^' + Err_m[1]+" "+`er` + '^alend;'
#	traceback.print_exc()

#os.system("cat ./html_head.txt "+Output_dir+"/sorted_"+Sample_id+"_FinalPOP.txt  ./html_tail.txt > "+Output_dir+"/html_graph_"+Sample_id+".html")
