##fileformat=VCFv4.1
##FORMAT=<ID=AD,Number=.,Type=Integer,Description="Allelic depths for the ref and alt alleles in the order listed">
##FORMAT=<ID=DP,Number=1,Type=Integer,Description="Approximate read depth (reads with MQ=255 or with bad mates are filtered)">
##FORMAT=<ID=GQ,Number=1,Type=Integer,Description="Genotype Quality">
##FORMAT=<ID=GT,Number=1,Type=String,Description="Genotype">
##FORMAT=<ID=PL,Number=G,Type=Integer,Description="Normalized, Phred-scaled likelihoods for genotypes as defined in the VCF specification">
##DRAGENCommandLine=<ID=dragen,Date="Mon Jan 11 20:27:11 PST 2016",CommandLineOptions=" -r /staging/human/reference/EDGC-hg19 --output-directory /staging/samples/MGB_WGS/KJS --output-file-prefix KJS -1 /staging/samples/MGB_WGS/NewSamples_20160107/1512KHX-0017_hdd1/KJS/KJS_R1.fastq.gz -2 /staging/samples/MGB_WGS/NewSamples_20160107/1512KHX-0017_hdd1/KJS/KJS_R2.fastq.gz --enable-map-align true --enable-map-align-output true --enable-bam-compression true --enable-sort true --enable-duplicate-marking true --remove-duplicates false --enable-bam-indexing true --RGID KJS --RGPL illumina --RGLB KJS --RGPU KJS --RGSM KJS --enable-variant-caller true --vc-reference /staging/human/reference/EDGC-hg19/M_hg19_ucsc_all.fa --vc-sample-name KJS --intermediate-results-dir /staging/tmp/ --dbsnp /staging/human/reference/EDGC-hg19/dbsnp_138.hg19.vcf --combine-samples-by-name false">
##DRAGENVersion=<ID=dragen,Version="SW: 01.002.060.01.02.14.13277, HW: 01.002.060">
##INFO=<ID=DP,Number=1,Type=Integer,Description="Approximate read depth; some reads may have been filtered">
##INFO=<ID=FS,Number=1,Type=Float,Description="Phred-scaled p-value using Fisher's exact test to detect strand bias">
##INFO=<ID=MQ,Number=1,Type=Float,Description="RMS Mapping Quality">
##INFO=<ID=MQRankSum,Number=1,Type=Float,Description="Z-score From Wilcoxon rank sum test of Alt vs. Ref read mapping qualities">
##INFO=<ID=QD,Number=1,Type=Float,Description="Variant Confidence/Quality by Depth">
##INFO=<ID=ReadPosRankSum,Number=1,Type=Float,Description="Z-score from Wilcoxon rank sum test of Alt vs. Ref read position bias">
##INFO=<ID=DB,Number=0,Type=Flag,Description="dbSNP Membership">
##FILTER=<ID=DRAGENHardSNP,Description="Set if true:QD < 2.0 || MQ < 40.0 || FS > 60.0 || MQRankSum < -12.5 || ReadPosRankSum < -8.0">
##FILTER=<ID=DRAGENHardINDEL,Description="Set if true:QD < 2.0 || ReadPosRankSum < -20.0 || FS > 200.0">
##contig=<ID=chrM,length=16571,assembly=M_hg19_ucsc_all>
##contig=<ID=chr1,length=249250621,assembly=M_hg19_ucsc_all>
##contig=<ID=chr2,length=243199373,assembly=M_hg19_ucsc_all>
##contig=<ID=chr3,length=198022430,assembly=M_hg19_ucsc_all>
##contig=<ID=chr4,length=191154276,assembly=M_hg19_ucsc_all>
##contig=<ID=chr5,length=180915260,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6,length=171115067,assembly=M_hg19_ucsc_all>
##contig=<ID=chr7,length=159138663,assembly=M_hg19_ucsc_all>
##contig=<ID=chr8,length=146364022,assembly=M_hg19_ucsc_all>
##contig=<ID=chr9,length=141213431,assembly=M_hg19_ucsc_all>
##contig=<ID=chr10,length=135534747,assembly=M_hg19_ucsc_all>
##contig=<ID=chr11,length=135006516,assembly=M_hg19_ucsc_all>
##contig=<ID=chr12,length=133851895,assembly=M_hg19_ucsc_all>
##contig=<ID=chr13,length=115169878,assembly=M_hg19_ucsc_all>
##contig=<ID=chr14,length=107349540,assembly=M_hg19_ucsc_all>
##contig=<ID=chr15,length=102531392,assembly=M_hg19_ucsc_all>
##contig=<ID=chr16,length=90354753,assembly=M_hg19_ucsc_all>
##contig=<ID=chr17,length=81195210,assembly=M_hg19_ucsc_all>
##contig=<ID=chr18,length=78077248,assembly=M_hg19_ucsc_all>
##contig=<ID=chr19,length=59128983,assembly=M_hg19_ucsc_all>
##contig=<ID=chr20,length=63025520,assembly=M_hg19_ucsc_all>
##contig=<ID=chr21,length=48129895,assembly=M_hg19_ucsc_all>
##contig=<ID=chr22,length=51304566,assembly=M_hg19_ucsc_all>
##contig=<ID=chrX,length=155270560,assembly=M_hg19_ucsc_all>
##contig=<ID=chrY,length=59373566,assembly=M_hg19_ucsc_all>
##contig=<ID=chr1_gl000191_random,length=106433,assembly=M_hg19_ucsc_all>
##contig=<ID=chr1_gl000192_random,length=547496,assembly=M_hg19_ucsc_all>
##contig=<ID=chr4_ctg9_hap1,length=590426,assembly=M_hg19_ucsc_all>
##contig=<ID=chr4_gl000193_random,length=189789,assembly=M_hg19_ucsc_all>
##contig=<ID=chr4_gl000194_random,length=191469,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6_apd_hap1,length=4622290,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6_cox_hap2,length=4795371,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6_dbb_hap3,length=4610396,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6_mann_hap4,length=4683263,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6_mcf_hap5,length=4833398,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6_qbl_hap6,length=4611984,assembly=M_hg19_ucsc_all>
##contig=<ID=chr6_ssto_hap7,length=4928567,assembly=M_hg19_ucsc_all>
##contig=<ID=chr7_gl000195_random,length=182896,assembly=M_hg19_ucsc_all>
##contig=<ID=chr8_gl000196_random,length=38914,assembly=M_hg19_ucsc_all>
##contig=<ID=chr8_gl000197_random,length=37175,assembly=M_hg19_ucsc_all>
##contig=<ID=chr9_gl000198_random,length=90085,assembly=M_hg19_ucsc_all>
##contig=<ID=chr9_gl000199_random,length=169874,assembly=M_hg19_ucsc_all>
##contig=<ID=chr9_gl000200_random,length=187035,assembly=M_hg19_ucsc_all>
##contig=<ID=chr9_gl000201_random,length=36148,assembly=M_hg19_ucsc_all>
##contig=<ID=chr11_gl000202_random,length=40103,assembly=M_hg19_ucsc_all>
##contig=<ID=chr17_ctg5_hap1,length=1680828,assembly=M_hg19_ucsc_all>
##contig=<ID=chr17_gl000203_random,length=37498,assembly=M_hg19_ucsc_all>
##contig=<ID=chr17_gl000204_random,length=81310,assembly=M_hg19_ucsc_all>
##contig=<ID=chr17_gl000205_random,length=174588,assembly=M_hg19_ucsc_all>
##contig=<ID=chr17_gl000206_random,length=41001,assembly=M_hg19_ucsc_all>
##contig=<ID=chr18_gl000207_random,length=4262,assembly=M_hg19_ucsc_all>
##contig=<ID=chr19_gl000208_random,length=92689,assembly=M_hg19_ucsc_all>
##contig=<ID=chr19_gl000209_random,length=159169,assembly=M_hg19_ucsc_all>
##contig=<ID=chr21_gl000210_random,length=27682,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000211,length=166566,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000212,length=186858,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000213,length=164239,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000214,length=137718,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000215,length=172545,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000216,length=172294,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000217,length=172149,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000218,length=161147,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000219,length=179198,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000220,length=161802,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000221,length=155397,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000222,length=186861,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000223,length=180455,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000224,length=179693,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000225,length=211173,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000226,length=15008,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000227,length=128374,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000228,length=129120,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000229,length=19913,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000230,length=43691,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000231,length=27386,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000232,length=40652,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000233,length=45941,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000234,length=40531,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000235,length=34474,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000236,length=41934,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000237,length=45867,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000238,length=39939,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000239,length=33824,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000240,length=41933,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000241,length=42152,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000242,length=43523,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000243,length=43341,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000244,length=39929,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000245,length=36651,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000246,length=38154,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000247,length=36422,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000248,length=39786,assembly=M_hg19_ucsc_all>
##contig=<ID=chrUn_gl000249,length=38502,assembly=M_hg19_ucsc_all>
##reference=file:///staging/human/reference/EDGC-hg19/M_hg19_ucsc_all.fa
#CHROM	POS	ID	REF	ALT	QUAL	FILTER	INFO	FORMAT	KJS
